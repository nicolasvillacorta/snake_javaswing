package SnakeTrying.SnakeProject;

import javax.swing.JFrame;

public class App 
{
    public App() {
    	
    	JFrame frame = new JFrame();
    	GamePanel gamepanel = new GamePanel();
    	
    	frame.add(gamepanel);
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setTitle("SNAKECODING");
    	
    	
    	frame.pack();
    	frame.setVisible(true);
    	
    }
    
    public static void main(String[] args) {
    	
    	new App();
    }
    
    
}
